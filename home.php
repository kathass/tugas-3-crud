<?php
	session_start();

	include "koneksi.php"; 

    if($_SESSION["login"] !== 1)
    	header("Location: index.php");

if (isset($_GET["pesan"])) {
	if ($_GET["pesan"] === "berhasil_hapus"){
		$pesan = "Berhasil Menghapus Data";
		$warna = "success";
	}
	if ($_GET["pesan"] === "berhasil_ubah"){
		$pesan = "Berhasil Mengubah Data";
		$warna = "success";
	}
	if ($_GET["pesan"] === "gagal_hapus"){
		$pesan = "Gagal Menghapus Data";
		$warna = "danger";
	}
}

    $username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

// Fetch all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Home</title>
</head>
<style>
	body{
		background-image: url('y.png');
		background-size: cover;
	}
	table {
		background-color: white;
		opacity: 72%;
	}
	footer {
		position: fixed;
		left: 0;
		bottom: 0;
		width: 100%;
		background-color: white;
		color: black;
		text-align: center;
		font-size: 20px;
	}
</style>
<body>
	<div class="container">
		<h1 style="background-color: white;">Data User</h1>
		<a class="btn btn-primary" href="registrasi.php">Tambah Data</a>

		<?php 
		if (isset($pesan)) {
			?>
			<div class="alert alert-<?= $warna; ?>" role="alert">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?= $pesan ?>
			</div>
			<?php
		}
		?>

		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama</th>
					<th>Username</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($hasil as $key => $user_data) {
				?>
				<tr>
					<td><?= $key + 1 ?></td>
					<td><?= $user_data["name"] ?></td>
					<td><?= $user_data["username"] ?></td>
					<td><?= $user_data["email"] ?></td>
					<td>
						<a class="btn btn-success"href="form_edit.php?id=<?=$user_data['id']?>">Edit</a>
						<a class="btn btn-danger"href="proses_delete.php?id=<?=$user_data['id']?>">Delete</a>
					</td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<br><br>
	<a class="btn btn-info" href="logout.php" class="lg">Log Out</a>

	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.5.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<footer>
  	<p align="center">Copyright @ 2021 Kathryna's</p>
</footer>
</html>