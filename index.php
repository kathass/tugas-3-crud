<?php 

if (isset($_GET["pesan"])){
    $pesan = $_GET["pesan"];
    } 
else {
    $pesan = "";
    }
if (isset($_SESSION["login.php"]) && $_SESSION["login"] === 1){
    header("Location: home.php");
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Login</title>
</head>
<body>
    <div class="center">
      <input type="checkbox" id="show">
      <label for="show" class="show-btn">Login</label>
      <div class="container">
        <label for="show" class="close-btn fas fa-times" title="close"></label>
        <div class="text">Login Form</div>
            <br>
        <?php echo $pesan; ?>

        <form method="POST" action="proses_login.php">
            <div class="data">
                <label>Username :</label>
                <input name="username" type="text" required>
            </div>
            <div class="data">
                <label>Password :</label>
                <input name="password" type="password" required>
            </div>
            <div class="btn">
                <div class="inner">  
                </div>
                    <button type="submit">LOGIN</button>       
            </div>
            <a href="registrasi.php">Registrasi</a>
        </form>
    </div>
</body>
</html>