<?php

if (isset($_GET["pesan"])){
    $pesan = $_GET["pesan"];
    } else {
    $pesan = " ";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<style>
    body{
  background-image: url('h.jpg');
  background-size: cover;
}
    .container{
  background: #fff;
  width: 55vh;
  padding: 25px;
  box-shadow: 0 0 8px rgba(0,0,0,0.1);
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-family: monospace;
}
.text{
  font-size: 35px;
  font-weight: 600;
  text-align: center;
  font-family: "century gothic";
  padding-bottom: 7px;
}
    form{
        padding-left: 85vh;
}
}
    .data{
        margin-top: 3px;
        padding: 3px 0;
}
    label{
  font-size: 18px;
}
    input{
  height: 100%;
  width: 98%;
  font-size: 17px;
  border: 1px solid silver;
  padding: 5.5px 0;
  margin-bottom: 3px;
}
.btn{
  margin: 30px 0;
  height: 45px;
  width: 100%;
  position: relative;
  overflow: hidden;
}
.inner{
  height: 100%;
  width: 300%;
  position: absolute;
  z-index: -1;
  background: -webkit-linear-gradient(right, #56d8e4, #9f01ea, #56d8e4, #9f01ea);
}
button{
  height: 100%;
  width: 100%;
  background: none;
  border: none;
  color: #fff;
  font-size: 20px;
  font-family: "century gothic"
}
    a {
        color: black;
        font-family: monospace;
        font-size: 18px; 
    }
</style>
<body>
    
    <br>
    <?php  echo $pesan; ?>

    <form method="POST" action="proses_register.php">
        <div class="container">
        <div class="text">Registerasi</div>
        <div class="data">
            <label> Nama :</label><br>
            <input name="name" type="text" required><br>
        </div>
        <div class="data">
            <label> Username :</label><br>
            <input name="username" type="text" required><br>
        </div>
        <div class="data">
            <label>Email :</label><br>
            <input name="email" type="email" required><br>
        </div>
        <div class="data">
            <label>Password :</label><br>
            <input name="password" type="password" required><br>
        </div>
        <div class="data">
            <label>Konfirmasi password :</label><br>
            <input name="konfirmasi_password" type="password" required><br>
        </div>
        
        <div class="btn">
            <div class="inner">  
            </div>
            <button type="submit">KIRIM</button><br>    
        </div>
            <a href="index.php">Login</a>
        </div>
    </form>
    
</body>
</html>